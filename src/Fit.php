<?php

/**
 * Description of Fit
 *
 * @author tiagohenrique
 * @version 0.0.2
 * @link https://bitbucket.org/tiagohenrique/fit-mini-framework
 */
class Fit
{

    const PDO_DRIVER_MYSQL = "mysql";

    protected static $autoInit = true;
    protected static $connections = array();
    protected static $layout = null;
    protected static $layoutTime = null;
    protected static $metas = array();
    protected static $routes = array();
    protected static $routeName = null;
    protected static $routesRegex = array();
    protected static $scripts = array();
    protected static $styles = array();
    protected static $template = null;
    protected static $title = null;
    protected static $view = array();

    public function __construct()
    {
        self::$layoutTime = time();
        self::$scripts = array(
            'body' => array(),
            'head' => array(),
        );
    }

    public function addConnection($driver, $database, $user, $pass = '', $host = '', $options = array())
    {
        self::$connections[0] = array(
            'driver' => $driver,
            'database' => $database,
            'user' => $user,
            'pass' => $pass,
            'host' => $host,
            'options' => $options,
        );
    }

    public function addRoute($name, array $config)
    {
        self::$routes[$name] = array(
            'name' => $name,
            'url' => $config['url'],
            'template' => $config['template'],
            'layout' => (isset($config['layout'])) ? $config['layout'] : self::getLayout(),
            'controller' => (isset($config['controller'])) ? $config['controller'] : null,
        );
    }

    public function addRouteRegex($name, array $config)
    {
        self::$routesRegex[$name] = array(
            'name' => $name,
            'pattern' => $config['regex']['pattern'],
            'params' => $config['regex']['params'],
            'template' => $config['template'],
            'layout' => (isset($config['layout'])) ? $config['layout'] : self::getLayout(),
            'controller' => (isset($config['controller'])) ? $config['controller'] : null,
        );
    }

    private function controller(array $config)
    {
        self::$routeName = $config['name'];

        if (null !== ($controller = $config['controller'])) {
            if (!file_exists($controller['file'])) {
                throw new Exception("Controller not found: {$controller['file']}");
            }

            require $controller['file'];
            $controller = new $controller['name'];
            if ((self::$autoInit) and ( method_exists($controller, '_init'))) {
                $controller->_init();
            }
        }

        $this->output($config['layout'], $config['template']);
    }

    public function disableAutoInit()
    {
        self::disableInit();
    }

    public static function disableInit()
    {
        self::$autoInit = false;
    }

    public static function getConnection()
    {
        $con = self::$connections[0];

        switch ($con['driver']) {
            case self::PDO_DRIVER_MYSQL:
                $dns = "{$con['driver']}:host={$con['host']};dbname={$con['database']}";
                return new PDO($dns, $con['user'], $con['pass'], $con['options']);
                break;

            default:
                throw new Exception("Driver indisponivel");
                break;
        }
    }

    public static function getBodyScript()
    {
        $time = self::$layoutTime;
        echo "{{BODY_SCRIPT_{$time}}}";
    }

    public static function getHeadMeta()
    {
        $time = self::$layoutTime;
        echo "{{HEAD_META_{$time}}}";
    }

    public static function getHeadScript()
    {
        $time = self::$layoutTime;
        echo "{{HEAD_SCRIPT_{$time}}}";
    }

    public static function getHeadStyle()
    {
        $time = self::$layoutTime;
        echo "{{HEAD_STYLE_{$time}}}";
    }

    public static function getHeadTitle()
    {
        $time = self::$layoutTime;
        echo "{{HEAD_TITLE_{$time}}}";
    }

    protected static function getLayout()
    {
        return self::$layout;
    }

    public static function getParam($param, $default = null)
    {
        if (isset($_GET[$param])) {
            return $_GET[$param];
        } elseif (isset($_POST[$param])) {
            return $_POST[$param];
        } else {
            return $default;
        }
    }

    public static function getParamGet($param, $default = null)
    {
        if (isset($_GET[$param])) {
            return $_GET[$param];
        } else {
            return $default;
        }
    }

    public static function getParamPost($param, $default = null)
    {
        if (isset($_POST[$param])) {
            return $_POST[$param];
        } else {
            return $default;
        }
    }

    public static function getRouteByName($routeName)
    {
        if (isset(self::$routes[$routeName])) {
            return self::$routes[$routeName];
        } elseif (isset(self::$routesRegex[$routeName])) {
            return self::$routesRegex[$routeName];
        } else {
            return null;
        }
    }

    public static function getRouteName()
    {
        return self::$routeName;
    }

    public static function getTemplate()
    {
        return self::$template;
    }

    public static function getView($variable = null)
    {
        if (null === $variable) {
            return self::$view;
        }

        if (array_key_exists($variable, self::$view)) {
            return self::$view[$variable];
        }

        throw new Exception("Variable not found: {$variable}");
    }

    public static function isAjax()
    {
        $ajax = filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH');
        return (strtolower($ajax) == 'xmlhttprequest');
    }

    public static function renderAjax($template)
    {
        return self::renderTemplatePartial($template);
    }

    protected static function renderLayout()
    {
        require self::getLayout();
    }

    public static function renderTemplate()
    {
        return self::renderTemplatePartial(self::getTemplate());
    }

    public static function renderTemplatePartial($template)
    {
        if (file_exists($template)) {
            require $template;
        } else {
            throw new Exception("File not found: {$template}");
        }
    }

    private function resolveRoute()
    {
        $requestUri = urldecode(trim(filter_input(INPUT_SERVER, 'REQUEST_URI'), "/"));

        foreach (self::$routes as $name => $config) {
            if ($config['url'] == $requestUri) {
                $this->controller($config);
                return;
            }
        }

        foreach (self::$routesRegex as $name => $config) {
            $matches = null;
            $pattern = $config['pattern'];
            $params = $config['params'];

            preg_match_all($pattern, $requestUri, $matches);

            if (empty($matches[0])) {
                continue;
            }

            self::$routeName = $name;
            $requestParams = array();
            foreach ($params as $key => $param) {

                $value = $matches[$key];

                if (count($value) > 1) {
                    $requestParams[$param] = $value;
                } else {
                    $requestParams[$param] = $value[0];
                }
            }

            if (!empty($requestParams)) {
                $_GET += $requestParams;
            }

            $this->controller($config);
            return;
        }

        header("HTTP/1.0 404 Not Found");
        throw new Exception('Nenhuma rota encontrada');
    }

    /**
     * Executa o framework
     */
    public function run()
    {
        $this->resolveRoute();
    }

    public static function setBodyScript($script, $after = true)
    {
        if ($after) {
            self::$scripts['body'][] = $script;
        } else {
            self::$scripts['body'] = array_merge(array($script), self::$scripts['body']);
        }
    }

    public static function setHeadMeta($meta)
    {
        self::$metas[] = $meta;
    }

    public static function setHeadScript($script, $after = true)
    {
        if ($after) {
            self::$scripts['head'][] = $script;
        } else {
            self::$scripts['head'] = array_merge(array($script), self::$scripts['head']);
        }
    }

    public static function setHeadStyle($style, $after = true)
    {
        if ($after) {
            self::$styles[] = $style;
        } else {
            self::$styles = array_merge(array($style), self::$styles);
        }
    }

    public static function setHeadTitle($title)
    {
        self::$title = $title;
    }

    /**
     * Define o layout padrao
     * @param string $layout Caminho para o layout padrao
     */
    public function setLayout($layout)
    {
        self::$layout = $layout;
    }

    public function setTemplate($template)
    {
        self::$template = $template;
    }

    private function output($layout, $template)
    {
        ob_start();

        $this->setTemplate($template);
        $this->setLayout($layout);

        if (self::isAjax()) {
            $output = self::renderAjax(self::getTemplate());
        } else {
            if (null !== self::getLayout()) {
                self::renderLayout();
                $output = ob_get_clean();
            } elseif (null !== self::getTemplate()) {
                self::renderTemplate();
                $output = ob_get_clean();
            }
        }

        $time = self::$layoutTime;

        $styles = '';
        foreach (self::$styles as $style) {
            $styles .= "<link href='{$style}' rel='stylesheet' type='text/css'>";
        }
        $output = str_replace("{{HEAD_STYLE_{$time}}}", $styles, $output);

        $scripts = '';
        foreach (self::$scripts['head'] as $script) {
            $scripts .= "<script src='{$script}'></script>";
        }
        $output = str_replace("{{HEAD_SCRIPT_{$time}}}", $scripts, $output);

        $scripts = '';
        foreach (self::$scripts['body'] as $script) {
            $scripts .= "<script src='{$script}'></script>";
        }
        $output = str_replace("{{BODY_SCRIPT_{$time}}}", $scripts, $output);

        $metas = '';
        foreach (self::$metas as $meta) {
            $attr = '';
            foreach ($meta as $key => $value) {
                $attr .= " {$key}='{$value}'";
            }
            $metas .= "<meta{$attr}>";
        }
        $output = str_replace("{{HEAD_META_{$time}}}", $metas, $output);

        $title = '';
        if (null !== self::$title) {
            $title = "<title>" . self::$title . "</title>";
        }
        $output = str_replace("{{HEAD_TITLE_{$time}}}", $title, $output);

        echo $output;
    }

    private function setView2($layout, $template)
    {
        $this->setTemplate($template);
        $this->setLayout($layout);

        if (self::isAjax()) {
            self::renderAjax(self::getTemplate());
        } else {
            if (null !== self::getLayout()) {
                self::renderLayout();
            } elseif (null !== self::getTemplate()) {
                self::renderTemplate();
            }
        }
    }

    public static function viewAssign($variable, $value)
    {
        self::$view[$variable] = $value;
    }

}
