# Fit - Mini Framework #

## Diretórios [Directories]

    /www
     -- .htaccess
     -- index.php
     -- Fit.php

__.htaccess__

    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} -s [OR]
    RewriteCond %{REQUEST_FILENAME} -l [OR]
    RewriteCond %{REQUEST_FILENAME} -d
    RewriteRule ^.*$ - [NC,L]
    RewriteRule ^.*$ index.php [NC,L]


__index.php__

    <?php
    require_once "Fit.php";
    $fit = new Fit();
    $fit->run();

## [Documentação [Documentation]](https://bitbucket.org/tiagohenrique/fit-mini-framework/wiki/Home)

## Mais informações [More info]

h8.tiago@gmail.com